package com.gitlab.soshibby.issuereporter.logaggregator;

import com.gitlab.soshibby.issuereporter.log.LogStatement;

import java.util.List;
import java.util.function.Consumer;

public interface LogAggregator {
    void init(String config);
    void start(Consumer<LogStatement> handler);
    void stop();

    List<LogStatement> getLogs(String requestId);
}
